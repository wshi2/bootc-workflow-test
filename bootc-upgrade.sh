#!/bin/bash
set -exuo pipefail

source tools/shared_lib.sh
dump_runner
# Used by local container registry which is configured 192.168.100.1 as ip address
deploy_libvirt_network

ARCH=$(uname -m)

# SSH configurations
SSH_OPTIONS=(-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o ConnectTimeout=5)
SSH_KEY=${TEMPDIR}/id_rsa
ssh-keygen -f "${SSH_KEY}" -N "" -q -t rsa-sha2-256 -b 2048

INSTALL_CONTAINERFILE=${TEMPDIR}/Containerfile.install
UPGRADE_CONTAINERFILE=${TEMPDIR}/Containerfile.upgrade
REGISTRY_IP="192.168.100.1"
REGISTRY_PORT=5000
TEST_IMAGE_NAME="bootc-workflow-test"
TEST_IMAGE_URL="${REGISTRY_IP}:${REGISTRY_PORT}/${TEST_IMAGE_NAME}:${SCENARIO}"

# Upgrade scenario
case "$SCENARIO" in
"9z-to-9y")
  DAY1_IMAGE_URL="registry.stage.redhat.io/rhel9/rhel-bootc:9.5"
  DAY2_IMAGE_URL="registry.stage.redhat.io/rhel9/rhel-bootc:9.6"
  ;;
"9y-to-10y")
  DAY1_IMAGE_URL="registry.stage.redhat.io/rhel9/rhel-bootc:9.6"
  DAY2_IMAGE_URL="registry.stage.redhat.io/rhel10/rhel-bootc:10.0"
  ;;
*)
  exit 1
  ;;
esac

# Create local container registry
greenprint "Generate certificate"
openssl req \
  -newkey rsa:4096 \
  -nodes \
  -sha256 \
  -keyout "${TEMPDIR}/domain.key" \
  -addext "subjectAltName = IP:${REGISTRY_IP}" \
  -x509 \
  -days 365 \
  -out "${TEMPDIR}/domain.crt" \
  -subj "/C=US/ST=Denial/L=Stockholm/O=bootc/OU=bootc-test/CN=bootc-test/emailAddress=bootc-test@bootc-test.org"

greenprint "Update CA Trust"
sudo cp "${TEMPDIR}/domain.crt" "/etc/pki/ca-trust/source/anchors/${REGISTRY_IP}.crt"
sudo update-ca-trust

greenprint "Deploy registry"
podman run \
  -d \
  --name registry \
  --replace \
  --network host \
  -v "${TEMPDIR}":/certs:z \
  -e REGISTRY_HTTP_ADDR="${REGISTRY_IP}:${REGISTRY_PORT}" \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
  -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
  quay.io/bootc-test/registry:2.8.3
podman ps -a

greenprint "Create installation Containerfile"
tee "$INSTALL_CONTAINERFILE" >/dev/null <<EOF
FROM "$DAY1_IMAGE_URL"
COPY domain.crt /etc/pki/ca-trust/source/anchors/${REGISTRY_IP}.crt
RUN update-ca-trust
EOF

greenprint "Check installation Containerfile"
cat "$INSTALL_CONTAINERFILE"

greenprint "Login registry.stage.redhat.io"
podman login \
  -u "${REGISTRY_STAGE_USERNAME}" \
  -p "${REGISTRY_STAGE_PASSWORD}" \
  registry.stage.redhat.io

greenprint "Build and push installation container image"
podman build \
  --tls-verify=false \
  --retry=5 \
  --retry-delay=10s \
  -t "${TEST_IMAGE_NAME}:${SCENARIO}" \
  -f "$INSTALL_CONTAINERFILE" \
  "$TEMPDIR"

podman push \
  --tls-verify=false \
  --quiet \
  "${TEST_IMAGE_NAME}:${SCENARIO}" \
  "$TEST_IMAGE_URL"

sudo truncate -s 10G "${TEMPDIR}/disk.raw"

sudo podman run \
  --rm \
  --privileged \
  --pid=host \
  --tls-verify=false \
  --security-opt label=type:unconfined_t \
  -v /var/lib/containers:/var/lib/containers \
  -v /dev:/dev \
  -v "$TEMPDIR":/output \
  "$TEST_IMAGE_URL" \
  bootc install to-disk \
  --filesystem "xfs" \
  --root-ssh-authorized-keys "/output/id_rsa.pub" \
  --karg=console=ttyS0,115200 \
  --generic-image \
  --via-loopback \
  /output/disk.raw

case "$ARCH" in
"aarch64")
  sudo qemu-system-aarch64 \
    -name bootc-vm \
    -enable-kvm \
    -machine virt \
    -cpu host \
    -m 2G \
    -bios /usr/share/AAVMF/AAVMF_CODE.fd \
    -drive file="${TEMPDIR}/disk.raw",if=virtio,format=raw \
    -net nic,model=virtio \
    -net user,hostfwd=tcp::2222-:22 \
    -display none \
    -daemonize
  ;;
"x86_64")
  sudo qemu-system-x86_64 \
    -name bootc-vm \
    -enable-kvm \
    -cpu host \
    -m 2G \
    -drive file="${TEMPDIR}/disk.raw",if=virtio,format=raw \
    -net nic,model=virtio \
    -net user,hostfwd=tcp::2222-:22 \
    -display none \
    -daemonize
  ;;
*)
  redprint "Only support x86_64 and aarch64"
  exit 1
  ;;
esac

wait_for_ssh_up() {
  SSH_STATUS=$(ssh "${SSH_OPTIONS[@]}" -i "${TEMPDIR}/id_rsa" -p 2222 root@"${1}" '/bin/bash -c "echo -n READY"')
  if [[ $SSH_STATUS == READY ]]; then
    echo 1
  else
    echo 0
  fi
}

for _ in $(seq 0 30); do
  RESULT=$(wait_for_ssh_up "localhost")
  if [[ $RESULT == 1 ]]; then
    echo "SSH is ready now! 🥳"
    break
  fi
  sleep 10
done

ssh "${SSH_OPTIONS[@]}" \
  -i "${TEMPDIR}/id_rsa" \
  -p 2222 \
  root@localhost \
  "bootc status"

greenprint "Create upgrade Containerfile"
tee "$UPGRADE_CONTAINERFILE" >/dev/null <<EOF
FROM "$DAY2_IMAGE_URL"
COPY domain.crt /etc/pki/ca-trust/source/anchors/${REGISTRY_IP}.crt
RUN update-ca-trust && \
    mkdir -p /usr/lib/bootc/kargs.d && \
    echo "kargs = [\"fips=1\"]" > /usr/lib/bootc/kargs.d/01-fips.toml && \
    update-crypto-policies --no-reload --set FIPS
EOF

greenprint "Build and push upgrade container image"
podman build \
  --tls-verify=false \
  --retry=5 \
  --retry-delay=10s \
  -t "${TEST_IMAGE_NAME}:${SCENARIO}" \
  -f "$UPGRADE_CONTAINERFILE" \
  "$TEMPDIR"

podman push \
  --tls-verify=false \
  --quiet \
  "${TEST_IMAGE_NAME}:${SCENARIO}" \
  "$TEST_IMAGE_URL"

ssh "${SSH_OPTIONS[@]}" \
  -i "${TEMPDIR}/id_rsa" \
  -p 2222 \
  root@localhost \
  "nohup bootc upgrade --quiet --apply & exit"
sleep 10

for _ in $(seq 0 30); do
  RESULT=$(wait_for_ssh_up "localhost")
  if [[ $RESULT == 1 ]]; then
    echo "SSH is ready now! 🥳"
    break
  fi
  sleep 10
done

ssh "${SSH_OPTIONS[@]}" \
  -i "${TEMPDIR}/id_rsa" \
  -p 2222 \
  root@localhost \
  "bootc status"

greenprint "🎉 All tests passed."
exit 0
