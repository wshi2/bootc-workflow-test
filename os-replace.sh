#!/bin/bash
set -exuo pipefail

source tools/shared_lib.sh
dump_runner
image_inspect

# SSH configurations
SSH_KEY=${TEMPDIR}/id_rsa
ssh-keygen -f "${SSH_KEY}" -N "" -q -t rsa-sha2-256 -b 2048
SSH_KEY_PUB="${SSH_KEY}.pub"

LAYERED_IMAGE="${LAYERED_IMAGE-cloud-init}"
# RHEL 10 and CS10 use cloud-init as gcp layered image
# No google-compute-engine package in RHEL 10 and CS10
if [[ "$LAYERED_IMAGE" == "gcp" ]]; then
  if [[ "$REDHAT_VERSION_ID" == "10.0" ]] || [[ "$REDHAT_VERSION_ID" == "10" ]]; then
    LAYERED_IMAGE="cloud-init"
  fi
fi
LAYERED_DIR="examples/$LAYERED_IMAGE"
INSTALL_CONTAINERFILE="$LAYERED_DIR/Containerfile"
UPGRADE_CONTAINERFILE=${TEMPDIR}/Containerfile.upgrade
QUAY_REPO_TAG="${QUAY_REPO_TAG:-$(
  tr -dc a-z0-9 </dev/urandom | head -c 4
  echo ''
)}"
INVENTORY_FILE="${TEMPDIR}/inventory"
AWS_BARE="${AWS_BARE-False}"
# only image type raw, qcow2, anaconda-iso and to-disk use PLATFORM=libvirt in tmt plan
# PLATFORM=libvirt -> use local registry -> no auth required in local registry
# local registry uses libvirt integration network gateway IP
if [[ "$PLATFORM" == "libvirt" ]]; then
  REGISTRY_IP="192.168.100.1"
  REGISTRY_PORT=5000
fi

# Do not enable FIPS except FIPS=enabled
FIPS="${FIPS:-disabled}"

# Only run RT kernel test in libvirt-rt
RT="${RT:-disabled}"

# Only run bootc kargs and LBI test in AWS
BOOTC_KARGS_LBI="${BOOTC_KARGS_LBI:-disabled}"

# bare PLATFORM uses aws bare instance
if [[ "$PLATFORM" == bare ]]; then
  PLATFORM="aws"
fi

# Only need quay.io login in non local registry or rhel-9.5-dev image
if [[ "$TIER1_IMAGE_URL" =~ rhel-9.5-dev ]] || [[ "$PLATFORM" != "libvirt" ]]; then
  greenprint "Login quay.io"
  podman login -u "${QUAY_USERNAME}" -p "${QUAY_PASSWORD}" quay.io
fi

REPLACE_CLOUD_USER=""
TEST_IMAGE_NAME="bootc-workflow-test"

case "$REDHAT_ID" in
"rhel")
  # rhel-bootc images are saved in registry.stage.redhat.io
  greenprint "Login registry.stage.redhat.io"
  podman login -u "${REGISTRY_STAGE_USERNAME}" -p "${REGISTRY_STAGE_PASSWORD}" registry.stage.redhat.io
  # work with old TEST_OS variable value rhel-9-x
  TEST_OS=$(echo "${REDHAT_ID}-${REDHAT_VERSION_ID}" | sed 's/\./-/')
  # RHEL image can be saved in private repo only
  TEST_IMAGE_URL="quay.io/redhat_emp1/${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}"
  SSH_USER="cloud-user"
  if [[ "$REDHAT_VERSION_ID" == *-beta ]]; then
    STAGE_POSTFIX="-Public-Beta"
  else
    STAGE_POSTFIX=""
  fi
  # use latest compose if specific compose is not accessible
  RC=$(curl -skIw '%{http_code}' -o /dev/null "http://${DOWNLOAD_NODE}/rhel-${REDHAT_VERSION_ID%%.*}/nightly/${BATCH_COMPOSE}RHEL-${REDHAT_VERSION_ID%%.*}${STAGE_POSTFIX}/${CURRENT_COMPOSE_ID}/STATUS")
  if [[ $RC != "200" ]]; then
    CURRENT_COMPOSE_ID=latest-RHEL-${REDHAT_VERSION_ID%%-beta}
  fi
  if [[ "$RT" == "enabled" ]]; then
    sed "s/REPLACE_ME/${DOWNLOAD_NODE}/; s/REPLACE_X/${REDHAT_VERSION_ID%%.*}/; s|REPLACE_BATCH_COMPOSE|${BATCH_COMPOSE}|; s/REPLACE_Y/${REDHAT_VERSION_ID%%.*}${STAGE_POSTFIX}/; s/REPLACE_COMPOSE_ID/${CURRENT_COMPOSE_ID}/" files/rhel-rt.template | tee "${LAYERED_DIR}"/rhel.repo >/dev/null
  else
    sed "s/REPLACE_ME/${DOWNLOAD_NODE}/; s/REPLACE_X/${REDHAT_VERSION_ID%%.*}/; s|REPLACE_BATCH_COMPOSE|${BATCH_COMPOSE}|; s/REPLACE_Y/${REDHAT_VERSION_ID%%.*}${STAGE_POSTFIX}/; s/REPLACE_COMPOSE_ID/${CURRENT_COMPOSE_ID}/" files/rhel.template | tee "${LAYERED_DIR}"/rhel.repo >/dev/null
  fi
  ADD_REPO="COPY rhel.repo /etc/yum.repos.d/rhel.repo"
  ADD_RHC="RUN dnf install -y rhc"
  if [[ "$PLATFORM" == "aws" ]]; then
    SSH_USER="ec2-user"
    REPLACE_CLOUD_USER='RUN sed -i "s/name: cloud-user/name: ec2-user/g" /etc/cloud/cloud.cfg'
  fi
  ;;
"centos")
  # work with old TEST_OS variable value centos-stream-9
  TEST_OS=$(echo "${REDHAT_ID}-${REDHAT_VERSION_ID}" | sed 's/-/-stream-/')
  TEST_IMAGE_URL="quay.io/bootc-test/${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}"
  SSH_USER="cloud-user"
  ADD_REPO=""
  ADD_RHC=""
  if [[ "$PLATFORM" == "aws" ]]; then
    SSH_USER="ec2-user"
    REPLACE_CLOUD_USER='RUN sed -i "s/name: cloud-user/name: ec2-user/g" /etc/cloud/cloud.cfg'
  fi
  ;;
"fedora")
  TEST_OS="${REDHAT_ID}-${REDHAT_VERSION_ID}"
  TEST_IMAGE_URL="quay.io/bootc-test/${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}"
  SSH_USER="fedora"
  ADD_REPO=""
  ADD_RHC=""
  ;;
*)
  redprint "Variable TIER1_IMAGE_URL is not supported"
  exit 1
  ;;
esac

greenprint "Configure container build arch"
case "$ARCH" in
"x86_64")
  BUILD_PLATFORM="linux/amd64"
  ;;
"aarch64")
  BUILD_PLATFORM="linux/arm64"
  ;;
"ppc64le")
  BUILD_PLATFORM="linux/ppc64le"
  ;;
"s390x")
  BUILD_PLATFORM="linux/s390x"
  ;;
*)
  redprint "Variable ARCH has to be defined"
  exit 1
  ;;
esac

if [[ ${AIR_GAPPED-} -eq 1 ]]; then
  AIR_GAPPED_DIR="$TEMPDIR"/virtiofs
  mkdir "$AIR_GAPPED_DIR"
else
  AIR_GAPPED=0
  AIR_GAPPED_DIR=""
fi

greenprint "Create $TEST_OS installation Containerfile"
sed -i "s|^FROM.*|FROM $TIER1_IMAGE_URL\n$ADD_REPO\n$ADD_RHC|" "$INSTALL_CONTAINERFILE"

if [[ "$LAYERED_IMAGE" == "qemu-guest-agent" ]]; then
  SSH_USER="root"
  SSH_KEY_PUB_CONTENT=$(cat "${SSH_KEY_PUB}")
  USER_CONFIG="RUN mkdir -p /usr/etc-system/ && echo 'AuthorizedKeysFile /usr/etc-system/%u.keys' >> /etc/ssh/sshd_config.d/30-auth-system.conf && \
       echo \"$SSH_KEY_PUB_CONTENT\" > /usr/etc-system/root.keys && chmod 0600 /usr/etc-system/root.keys"
  REPLACE_CLOUD_USER=""
elif [[ "$LAYERED_IMAGE" == "azure" ]]; then
  sed -i '/cloud.cfg/d' "$INSTALL_CONTAINERFILE"
elif [[ "$LAYERED_IMAGE" == "useradd-ssh" ]]; then
  sed -i "s|exampleuser|$SSH_USER|g" "$INSTALL_CONTAINERFILE"
fi

# Enable FIPS in Containerfile
if [[ "$FIPS" == "enabled" ]]; then
  tee -a "$INSTALL_CONTAINERFILE" >/dev/null <<EOF
RUN dnf install -y crypto-policies-scripts && mkdir -p /usr/lib/bootc/kargs.d && \
    echo "kargs = [\"fips=1\"]" > /usr/lib/bootc/kargs.d/01-fips.toml && \
    update-crypto-policies --no-reload --set FIPS
EOF
fi

# Replace to RT kernel
if [[ "$RT" == "enabled" ]]; then
  tee -a "$INSTALL_CONTAINERFILE" >/dev/null <<EOF
RUN dnf remove -y kernel kernel-core kernel-modules kernel-modules-core && dnf install -y kernel-rt && dnf -y clean all
EOF
fi

# only image type raw, qcow2, anaconda-iso and to-disk use PLATFORM=libvirt in tmt plan
# PLATFORM=libvirt -> use local registry -> no auth required in local registry
if [[ "$PLATFORM" == "libvirt" ]]; then
  # Init libvirt network
  greenprint "Deploy libvirt network"
  deploy_libvirt_network

  greenprint "Generate certificate"
  openssl req \
    -newkey rsa:4096 \
    -nodes \
    -sha256 \
    -keyout "${TEMPDIR}/domain.key" \
    -addext "subjectAltName = IP:${REGISTRY_IP}" \
    -x509 \
    -days 365 \
    -out "${TEMPDIR}/domain.crt" \
    -subj "/C=US/ST=Denial/L=Stockholm/O=bootc/OU=bootc-test/CN=bootc-test/emailAddress=bootc-test@bootc-test.org"

  greenprint "Update CA Trust"
  sudo cp "${TEMPDIR}/domain.crt" "/etc/pki/ca-trust/source/anchors/${REGISTRY_IP}.crt"
  sudo update-ca-trust

  greenprint "Deploy registry"
  podman run \
    -d \
    --name registry \
    --replace \
    --network host \
    -v "${TEMPDIR}":/certs:z \
    -e REGISTRY_HTTP_ADDR="${REGISTRY_IP}:${REGISTRY_PORT}" \
    -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
    -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
    quay.io/bootc-test/registry:2.8.3
  podman ps -a

  cp "${TEMPDIR}/domain.crt" "$LAYERED_DIR"
  tee -a "$INSTALL_CONTAINERFILE" >/dev/null <<EOF
COPY domain.crt /etc/pki/ca-trust/source/anchors/${REGISTRY_IP}.crt
RUN mv /etc/selinux /etc/selinux.tmp
RUN mv /etc/selinux.tmp /etc/selinux
RUN update-ca-trust && \
    dnf -y clean all && \
    rm -rf /var/cache /var/lib/dnf
$USER_CONFIG
EOF

  TEST_IMAGE_URL="${REGISTRY_IP}:${REGISTRY_PORT}/${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}"
  NO_PODMAN_AUTH="true"
else
  [[ $- =~ x ]] && debug=1 && set +x
  sed "s/REPLACE_ME/${QUAY_SECRET}/g" files/auth.template | tee "${LAYERED_DIR}"/auth.json >/dev/null
  [[ $debug == 1 ]] && set -x

  tee -a "$INSTALL_CONTAINERFILE" >/dev/null <<EOF
RUN dnf -y clean all && \
    rm -rf /var/cache /var/lib/dnf
COPY auth.json /etc/ostree/auth.json
$REPLACE_CLOUD_USER
EOF
  NO_PODMAN_AUTH="false"
fi

# examples/included-ssh-pubkey includes root public key
if [[ "$LAYERED_IMAGE" == "included-ssh-pubkey" ]]; then
  SSH_USER="root"
fi

# kargs and LBI test + zstd:chunked test
# fedora 40 and 41 has old podman which has zstd:chunked bug
if [[ "$PLATFORM" == "aws" ]] && [[ "$TEST_OS" != fedora-41 ]] && [[ "$TEST_OS" != fedora-40 ]]; then
  BOOTC_KARGS_LBI="enabled"

  # curl-base:latest is zstd:chunked compressed image
  tee -a "$INSTALL_CONTAINERFILE" >/dev/null <<EOF
RUN mkdir -p /usr/lib/bootc/kargs.d/
RUN cat <<KARGEOF >> /usr/lib/bootc/kargs.d/20-mitigation.toml
kargs = ["mitigations=auto,nosmt"]
KARGEOF

RUN cat <<CURLIMAGEEOF >> /usr/share/containers/systemd/curl-base.image
[Image]
Image=quay.io/bootc-test/curl-base:latest
CURLIMAGEEOF

RUN cat <<CURLCONTAINEREOF >> /usr/share/containers/systemd/curl.container
[Container]
Image=quay.io/curl/curl:latest
GlobalArgs=--storage-opt=additionalimagestore=/usr/lib/bootc/storage
CURLCONTAINEREOF

RUN cat <<PODMANIMAGEEOF >> /usr/share/containers/systemd/podman.image
[Image]
Image=registry.access.redhat.com/ubi9/podman:latest
PODMANIMAGEEOF

RUN ln -s /usr/share/containers/systemd/curl.container /usr/lib/bootc/bound-images.d/curl.container && \
    ln -s /usr/share/containers/systemd/curl-base.image /usr/lib/bootc/bound-images.d/curl-base.image && \
    ln -s /usr/share/containers/systemd/podman.image /usr/lib/bootc/bound-images.d/podman.image
EOF
fi

greenprint "Check $TEST_OS installation Containerfile"
cat "$INSTALL_CONTAINERFILE"

greenprint "Build $TEST_OS installation container image"
if [[ "$LAYERED_IMAGE" == "useradd-ssh" || "$LAYERED_IMAGE" == "included-ssh-pubkey" ]]; then
  podman build --platform "$BUILD_PLATFORM" --tls-verify=false --retry=5 --retry-delay=10s --label="quay.expires-after=8h" --build-arg "sshpubkey=$(cat "${SSH_KEY_PUB}")" -t "${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}" "$LAYERED_DIR"
else
  podman build --platform "$BUILD_PLATFORM" --tls-verify=false --retry=5 --retry-delay=10s --label="quay.expires-after=8h" -t "${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}" "$LAYERED_DIR"
fi

greenprint "Push $TEST_OS installation container image"
retry podman push --tls-verify=false --quiet "${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}" "$TEST_IMAGE_URL"

greenprint "Prepare inventory file"
tee -a "$INVENTORY_FILE" >/dev/null <<EOF
[cloud]
localhost

[guest]

[cloud:vars]
ansible_connection=local

[guest:vars]
ansible_user="$SSH_USER"
ansible_private_key_file="$SSH_KEY"
ansible_ssh_common_args="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"

[all:vars]
ansible_python_interpreter=/usr/bin/python3
EOF

greenprint "Prepare ansible.cfg"
export ANSIBLE_CONFIG="${PWD}/playbooks/ansible.cfg"

greenprint "Deploy $PLATFORM instance"
ansible-playbook -v \
  -i "$INVENTORY_FILE" \
  -e test_os="$TEST_OS" \
  -e ssh_user="$SSH_USER" \
  -e ssh_key_pub="$SSH_KEY_PUB" \
  -e inventory_file="$INVENTORY_FILE" \
  -e air_gapped_dir="$AIR_GAPPED_DIR" \
  -e layered_image="$LAYERED_IMAGE" \
  -e aws_bare="$AWS_BARE" \
  "playbooks/deploy-${PLATFORM}.yaml"

greenprint "Install $TEST_OS bootc system"
ansible-playbook -v \
  -i "$INVENTORY_FILE" \
  -e test_os="$TEST_OS" \
  -e test_image_url="$TEST_IMAGE_URL" \
  -e no_podman_auth="$NO_PODMAN_AUTH" \
  -e bootc_kargs_lbi="$BOOTC_KARGS_LBI" \
  playbooks/install.yaml

greenprint "Run ostree checking test on $PLATFORM instance"
ansible-playbook -v \
  -i "$INVENTORY_FILE" \
  -e test_os="$TEST_OS" \
  -e bootc_image="$TEST_IMAGE_URL" \
  -e layered_image="$LAYERED_IMAGE" \
  -e image_label_version_id="${REDHAT_VERSION_ID%-beta}" \
  -e fips="$FIPS" \
  -e bootc_kargs_lbi="$BOOTC_KARGS_LBI" \
  playbooks/check-system.yaml

greenprint "Create upgrade Containerfile"
tee "$UPGRADE_CONTAINERFILE" >/dev/null <<EOF
FROM "$TEST_IMAGE_URL"
RUN dnf -y install wget && \
    dnf -y clean all
EOF

greenprint "Build $TEST_OS upgrade container image"
podman build --platform "$BUILD_PLATFORM" --tls-verify=false --retry=5 --retry-delay=10s --label="quay.expires-after=8h" -t "${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}" -f "$UPGRADE_CONTAINERFILE" .
greenprint "Push $TEST_OS upgrade container image"
retry podman push --tls-verify=false --quiet "${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}" "$TEST_IMAGE_URL"

if [[ ${AIR_GAPPED-} -eq 1 ]]; then
  retry skopeo copy docker://"$TEST_IMAGE_URL" dir://"$AIR_GAPPED_DIR"
  BOOTC_IMAGE="/mnt"
else
  BOOTC_IMAGE="$TEST_IMAGE_URL"
fi

greenprint "Upgrade $TEST_OS system"
ansible-playbook -v \
  -i "$INVENTORY_FILE" \
  -e air_gapped_dir="$AIR_GAPPED_DIR" \
  playbooks/upgrade.yaml

greenprint "Run ostree checking test after upgrade on $PLATFORM instance"
ansible-playbook -v \
  -i "$INVENTORY_FILE" \
  -e test_os="$TEST_OS" \
  -e bootc_image="$BOOTC_IMAGE" \
  -e image_label_version_id="${REDHAT_VERSION_ID%-beta}" \
  -e fips="$FIPS" \
  -e upgrade="true" \
  -e bootc_kargs_lbi="$BOOTC_KARGS_LBI" \
  playbooks/check-system.yaml

greenprint "Rollback $TEST_OS system"
ansible-playbook -v \
  -i "$INVENTORY_FILE" \
  -e air_gapped_dir="$AIR_GAPPED_DIR" \
  playbooks/rollback.yaml

greenprint "Clean up"
rm -rf auth.json "${LAYERED_DIR}/rhel.repo"
unset ANSIBLE_CONFIG
podman rm -af

greenprint "🎉 All tests passed."
exit 0
