#!/bin/bash

cd ../../

if [ "$TEST_CASE" = "os-replace" ]; then
    ./os-replace.sh
elif [ "$TEST_CASE" = "anaconda" ]; then
    ./anaconda.sh
elif [ "$TEST_CASE" = "bib-image" ]; then
    ./bib-image.sh
elif [ "$TEST_CASE" = "run-image" ]; then
    ./run-image.sh
elif [ "$TEST_CASE" = "bootc-upgrade" ]; then
    ./bootc-upgrade.sh
elif [ "$TEST_CASE" = "bootc-switch" ]; then
    ./bootc-switch.sh
else
    echo "Error: Test case $TEST_CASE not found!"
    exit 1
fi
