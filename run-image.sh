#!/bin/bash

# This script is to debug bootc image and run sanity test for bootc image
# The only required arg for this script is TIER1_IMAGE_URL
# TIER1_IMAGE_URL: bootc image URL to be tested and debugged
# Login VM: ssh -i id_rsa -p 2222 root@localhost

set -exuo pipefail

TEMPDIR=$(mktemp -d)
trap 'rm -rf -- "$TEMPDIR"' EXIT

NEED_AUTH="${NEED_AUTH:-false}"

TIER1_IMAGE_URL="${TIER1_IMAGE_URL:-quay.io/fedora/fedora-bootc:40}"
BIB_IMAGE_URL="quay.io/centos-bootc/bootc-image-builder:latest"
SSH_OPTIONS=(-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o ConnectTimeout=5)
SSH_KEY=${TEMPDIR}/id_rsa
ssh-keygen -f "${SSH_KEY}" -N "" -q -t rsa-sha2-256 -b 2048
SSH_KEY_PUB="${SSH_KEY}.pub"
SSH_KEY_PUB_CONTENT=$(cat "$SSH_KEY_PUB")
ARCH=$(uname -m)

if [[ "$NEED_AUTH" == "true" ]]; then
    LOGIN_URL=$(echo "$TIER1_IMAGE_URL" | cut -d '/' -f 1)
    if [[ "$LOGIN_URL" == "quay.io" ]]; then
        REGISTRY_USERNAME="$QUAY_USERNAME"
        REGISTRY_PASSWORD="$QUAY_PASSWORD"
    fi
    if [[ "$LOGIN_URL" == "registry.stage.redhat.io" ]]; then
        REGISTRY_USERNAME="$REGISTRY_STAGE_USERNAME"
        REGISTRY_PASSWORD="$REGISTRY_STAGE_PASSWORD"
    fi
    sudo podman login -u "${REGISTRY_USERNAME}" -p "${REGISTRY_PASSWORD}" "$LOGIN_URL"
fi

sudo podman pull --quiet --tls-verify=false "$TIER1_IMAGE_URL"

tee -a "${TEMPDIR}/config.json" > /dev/null << EOF
{
  "customizations": {
    "user": [
      {
        "name": "root",
        "home": "/var/roothome",
        "key": "$SSH_KEY_PUB_CONTENT"
      }
    ]
  }
}
EOF

# bootc image builder 9.4 does not support --rootfs
ROOTFS_LIST=( \
    "ext4" \
    "xfs" \
)
RND_LINE=$((RANDOM % 2))
ROOTFS="${ROOTFS_LIST[$RND_LINE]}"

sudo podman run \
    --rm \
    -it \
    --privileged \
    --pull=newer \
    --tls-verify=false \
    --security-opt label=type:unconfined_t \
    -v "${TEMPDIR}/config.json":/config.json:ro \
    -v "$TEMPDIR":/output \
    -v /var/lib/containers/storage:/var/lib/containers/storage \
    "$BIB_IMAGE_URL" \
    --type raw \
    --tls-verify=false \
    --chown "$(id -u "$(whoami)"):$(id -g "$(whoami)")" \
    --rootfs "$ROOTFS" \
    --local \
    "$TIER1_IMAGE_URL"

case "$ARCH" in
    "aarch64")
        sudo qemu-system-aarch64 \
            -name bootc-vm \
            -enable-kvm \
            -machine virt \
            -cpu host \
            -m 2G \
            -bios /usr/share/AAVMF/AAVMF_CODE.fd \
            -drive file="${TEMPDIR}/image/disk.raw",if=virtio,format=raw \
            -net nic,model=virtio \
            -net user,hostfwd=tcp::2222-:22 \
            -display none \
            -daemonize
        ;;
    "x86_64")
        sudo qemu-system-x86_64 \
            -name bootc-vm \
            -enable-kvm \
            -cpu host \
            -m 2G \
            -drive file="${TEMPDIR}/image/disk.raw",if=virtio,format=raw \
            -net nic,model=virtio \
            -net user,hostfwd=tcp::2222-:22 \
            -display none \
            -daemonize
        ;;
    *)
        redprint "Only support x86_64 and aarch64"
        exit 1
        ;;
esac

wait_for_ssh_up () {
    SSH_STATUS=$(ssh "${SSH_OPTIONS[@]}" -i "${TEMPDIR}/id_rsa" -p 2222 root@"${1}" '/bin/bash -c "echo -n READY"')
    if [[ $SSH_STATUS == READY ]]; then
        echo 1
    else
        echo 0
    fi
}

for _ in $(seq 0 30); do
    RESULT=$(wait_for_ssh_up "localhost")
    if [[ $RESULT == 1 ]]; then
        echo "SSH is ready now! 🥳"
        break
    fi
    sleep 10
done

ssh "${SSH_OPTIONS[@]}" -i "${TEMPDIR}/id_rsa" -p 2222 root@localhost "bootc status"
